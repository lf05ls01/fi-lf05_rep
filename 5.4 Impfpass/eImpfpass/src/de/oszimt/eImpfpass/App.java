package de.oszimt.eImpfpass;

import java.awt.EventQueue;

import de.oszimt.eImpfpass.controller.MainController;
import de.oszimt.eImpfpass.model.MainModel;
import de.oszimt.eImpfpass.view.MainWindow;

public class App {

	public static void main(String[] args) {
		// Assemble all the pieces of the MVC

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				MainModel m = new MainModel();
				MainWindow w = new MainWindow();
				MainController c = new MainController(m, w);
				c.init();
			}
		});

	}
}
