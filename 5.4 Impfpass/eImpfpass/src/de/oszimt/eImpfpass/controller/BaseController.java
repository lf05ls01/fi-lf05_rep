package de.oszimt.eImpfpass.controller;

public abstract class BaseController {

	protected MainController controller;

	public BaseController(MainController controller) {
		super();
		this.controller = controller;
	}

	protected abstract void initView();

	protected abstract void initController();

	protected abstract void initModel();
}
