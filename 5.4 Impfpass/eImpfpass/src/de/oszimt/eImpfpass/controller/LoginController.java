/**
 *
 */
package de.oszimt.eImpfpass.controller;

import de.oszimt.eImpfpass.model.Doctor;
import de.oszimt.eImpfpass.view.LoginPanel;

/**
 * @author sven
 *
 */
public class LoginController extends BaseController {

	private LoginPanel panel;

	public LoginController(MainController controller) {
		super(controller);
	}

	@Override
	protected void initView() {
		panel = new LoginPanel();

		controller.getWindow().setActivePanel("Login", panel);

		panel.getLoginName().addActionListener(e -> login());
		panel.getLoginPassword().addActionListener(e -> login());
		panel.getLoginButton().addActionListener(e -> login());
		panel.getRegisterButton().addActionListener(e -> register());
	}

	/**
	 * Log a doctor in
	 */
	public void login() {
		// login
		final Doctor arzt = controller.getModel().getArzt().login(Integer.parseInt(panel.getLoginName().getText()),
				panel.getLoginPassword().getText());

		controller.setActiveUser(arzt);
		controller.setController(new PatientOverviewController(controller));
	}

	@Override
	protected void initController() {
		// show loginpanel
		initView();

		// skip it
		//panel.getLoginName().setText("3001");
		//panel.getLoginPassword().setText("3001");
		//login();
	}

	@Override
	protected void initModel() {
		// TODO Auto-generated method stub

	}

	/**
	 * Check if the entered data is allowed
	 *
	 * @return true, can login
	 */
	private boolean checkData() {
		// entered something?
		if (panel.getLoginName().getText().isBlank()) {
			panel.setError("Bitte geben Sie ihre ArztID ein.");
			return false;
		}

		try {
			Integer.parseInt(panel.getLoginName().getText());
		} catch (final NumberFormatException e) {
			panel.setError("Bitte geben Sie eine Zahl als ArztID ein.");
			return false;
		}

		if (panel.getLoginPassword().getText().isBlank()) {
			panel.setError("Bitte geben Sie ein Passwort ein.");
			return false;
		}

		return true;
	}

	public void register() {
		if (!checkData()) {
			return;
		}

		// register
		try {
			controller.getModel().getArzt().register(Integer.parseInt(panel.getLoginName().getText()),
					panel.getLoginPassword().getText());

			controller.setController(new PatientOverviewController(controller));
		} catch (final Exception e) {
			panel.setError(e.getLocalizedMessage());
			e.printStackTrace();
		}
	}

}
