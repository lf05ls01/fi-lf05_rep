package de.oszimt.eImpfpass.controller;

import de.oszimt.eImpfpass.model.Doctor;
import de.oszimt.eImpfpass.model.MainModel;
import de.oszimt.eImpfpass.view.MainWindow;

public class MainController {

	private MainWindow window;

	private MainModel model;
	private BaseController controller;

	private Doctor activeUser;

	public MainController(MainModel model, MainWindow window) {
		super();
		this.window = window;
		this.model = model;
	}

	public MainWindow getWindow() {
		return window;
	}

	public MainModel getModel() {
		return model;
	}

	public void setController(BaseController controller) {
		this.controller = controller;
		this.controller.initController();
	}

	public void init() {
		model.init();
		window.init();

		setController(new LoginController(this));
	}

	/**
	 * @return the activeUser
	 */
	public Doctor getActiveUser() {
		return activeUser;
	}

	/**
	 * @param activeUser the activeUser to set
	 */
	public void setActiveUser(Doctor activeUser) {
		this.activeUser = activeUser;
	}
}
