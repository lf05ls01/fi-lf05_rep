package de.oszimt.eImpfpass.controller;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;

import de.oszimt.eImpfpass.model.Patient;
import de.oszimt.eImpfpass.view.PatientEditPanel;

public class PatientEditController extends BaseController {

	private PatientEditPanel panel;
	private final Patient patient;

	public PatientEditController(MainController controller, Patient patient) {
		super(controller);

		this.patient = patient;
	}

	@Override
	protected void initView() {
		panel = new PatientEditPanel();

		// has a patient?
		if (patient != null) {
			panel.getId().setText(Integer.toString(patient.getPlz()));
			panel.getVorname().setText(patient.getVorname());
			panel.getLastName().setText(patient.getName());
			panel.getBirthday().setText(patient.getBirthday().toString());
			panel.getTown().setText(patient.getTown());
			panel.getStreet().setText(patient.getStreet());
		} else {
			panel.getId().setText("Wird beim Speichern erstellt");
			panel.getSaveButton().setText("Erstellen");
		}

		// connect it
		panel.getSaveButton().addActionListener(e -> save());
		panel.getBackButton().addActionListener(e -> back());

		final String hallo = patient == null ? "Neuen Patienten erstellen"
				: "Patient " + patient.getFullName() + " editieren";
		controller.getWindow().setActivePanel(hallo, panel);

	}

	@Override
	protected void initController() {
		// TODO Auto-generated method stub
		initView();
	}

	@Override
	protected void initModel() {
		// TODO Auto-generated method stub

	}

	/**
	 * Check if the entered data is allowed
	 *
	 * @return true, can login
	 */
	private boolean checkData() {
		// entered something?
		if (panel.getVorname().getText().isBlank()) {
			panel.setError("Bitte geben Sie einen Vornamen ein.");
			return false;
		}

		if (panel.getLastName().getText().isBlank()) {
			panel.setError("Bitte geben Sie ein Nachnamen ein.");
			return false;
		}

		if (panel.getBirthday().getText().isBlank()) {
			panel.setError("Bitte geben Sie einen Geburtstag ein.");
			return false;
		}

		try {
			LocalDate.parse(panel.getBirthday().getText());
		} catch (final DateTimeParseException e) {
			panel.setError("Bitte geben Sie einen gültigen Geburtstag ein.");
		}

		return true;
	}

	protected void back() {
		controller.setController(new PatientOverviewController(controller));
	}

	protected void save() {
		if (!checkData()) {
			return;
		}

		try {
			// update?
			if (patient != null) {
				patient.setVorname(panel.getVorname().getText());
				patient.setName(panel.getLastName().getText());
				patient.setBirthday(LocalDate.parse(panel.getBirthday().getText()));
				patient.setTown(panel.getTown().getText());
				patient.setStreet(panel.getStreet().getText());

				controller.getModel().getPatient().update(patient);
			} else {
				controller.getModel().getPatient().create(panel.getLastName().getText(), panel.getVorname().getText(),
						LocalDate.parse(panel.getBirthday().getText()), panel.getStreet().getText(), 0,
						panel.getTown().getText());
			}
		} catch (final SQLException e) {
			panel.setError(e.getLocalizedMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}

		back();
	}

}
