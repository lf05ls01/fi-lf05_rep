package de.oszimt.eImpfpass.controller;

import java.sql.SQLException;
import java.util.List;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import de.oszimt.eImpfpass.model.Patient;
import de.oszimt.eImpfpass.view.PatientOverviewPanel;
import de.oszimt.eImpfpass.view.PatientOverviewTableModel;

public class PatientOverviewController extends BaseController {

	private PatientOverviewPanel panel;
	private List<Patient> patienten;

	public PatientOverviewController(MainController controller) {
		super(controller);
	}

	@Override
	protected void initView() {
		panel = new PatientOverviewPanel();
		try {
			patienten = controller.getModel().getPatient().getAll();
		} catch (final SQLException e) {
			panel.setError(e.getLocalizedMessage());
			e.printStackTrace();
		}
		panel.getTable().setModel(new PatientOverviewTableModel(patienten));

		final String hallo = "Hallo " + controller.getActiveUser().getFullName() + ",";
		controller.getWindow().setActivePanel(hallo + " Ihre Patientenübersicht", panel);

		// add changer
		panel.getTable().getSelectionModel().addListSelectionListener(new RowListener());

		// add actions
		panel.getNewButton().addActionListener(e -> newPatient());
		panel.getEditButton().addActionListener(e -> editPatient());
		panel.getVacButton().addActionListener(e -> vacPatient());
		panel.getLogoutButton().addActionListener(e -> logoutPatient());

	}

	@Override
	protected void initController() {
		// TODO Auto-generated method stub
		initView();
	}

	@Override
	protected void initModel() {
		// TODO Auto-generated method stub

	}

	private Patient getSelected() {
		return patienten.get(panel.getTable().getSelectedRow());
	}

	private void newPatient() {
		controller.setController(new PatientEditController(controller, null));
	}

	private void editPatient() {
		try {
			controller.setController(new PatientEditController(controller, getSelected()));
		} catch (Throwable t){
			panel.setError(t.getLocalizedMessage());
		}
	}

	private void vacPatient() {
		controller.setController(new PatientVaccineOverviewController(controller, getSelected()));
	}

	private void logoutPatient() {
		controller.setActiveUser(null);
		controller.setController(new LoginController(controller));
	}

	private class RowListener implements ListSelectionListener {
		@Override
		public void valueChanged(ListSelectionEvent event) {
			if (event.getValueIsAdjusting()) {
				return;
			}
			final Patient p = getSelected();
			panel.getEditButton().setEnabled(true);
			panel.getEditButton().setText("Editiere " + p.getName());
			panel.getVacButton().setEnabled(true);
			panel.getVacButton().setText("Impfe " + p.getName());
		}
	}
}
