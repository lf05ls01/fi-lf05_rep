package de.oszimt.eImpfpass.controller;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;

import de.oszimt.eImpfpass.model.Vaccine;
import de.oszimt.eImpfpass.model.Patient;
import de.oszimt.eImpfpass.view.PatientVaccineEditPanel;

public class PatientVaccineEditController extends BaseController {

	private PatientVaccineEditPanel panel;
	private Patient patient;

	public PatientVaccineEditController(MainController controller, Patient patient) {
		super(controller);

		this.patient = patient;
	}

	@Override
	protected void initView() {
		panel = new PatientVaccineEditPanel();

		// set data
		panel.getPatient().setText(patient.getFullName());
		panel.getDoctor().setText(controller.getActiveUser().getFullName());

		try {
			for (Vaccine vac : controller.getModel().getImpfung().getAllNotPatient(patient.getId())) {
				panel.getVac().addItem(vac);
			}
		} catch (SQLException e1) {
			panel.setError(e1.getLocalizedMessage());
			e1.printStackTrace();
		}

		panel.getDate().setText(LocalDate.now().toString());

		// connect it
		panel.getSaveButton().addActionListener(e -> save());
		panel.getBackButton().addActionListener(e -> back());

		controller.getWindow().setActivePanel("Patient " + patient.getFullName() + " impfen", panel);

	}

	@Override
	protected void initController() {
		// TODO Auto-generated method stub
		initView();
	}

	@Override
	protected void initModel() {
		// TODO Auto-generated method stub

	}

	/**
	 * Check if the entered data is allowed
	 * 
	 * @return true, can login
	 */
	private boolean checkData() {
		// entered something?
		if (panel.getVac().getSelectedItem() == null) {
			panel.setError("Bitte Impfung auswählen.");
			return false;
		}

		try {
			LocalDate.parse(panel.getDate().getText());
		} catch (DateTimeParseException e) {
			panel.setError("Bitte geben Sie ein gültiges Datum ein.");
		}

		return true;
	}

	protected void back() {
		controller.setController(new PatientVaccineOverviewController(controller, patient));
	}

	protected void save() {
		if (!checkData()) {
			return;
		}

		try {
			controller.getModel().getPatientImpfung().Create(patient.getId(),
					((Vaccine) panel.getVac().getSelectedItem()).getiID(), controller.getActiveUser().getaID(),
					LocalDate.parse(panel.getDate().getText()));

		} catch (SQLException e) {
			panel.setError(e.getLocalizedMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}

		back();
	}

}
