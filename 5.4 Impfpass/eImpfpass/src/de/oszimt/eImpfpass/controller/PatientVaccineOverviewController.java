package de.oszimt.eImpfpass.controller;

import java.sql.SQLException;
import java.util.List;

import de.oszimt.eImpfpass.model.Patient;
import de.oszimt.eImpfpass.model.PatientVaccine;
import de.oszimt.eImpfpass.view.PatientVaccineOverviewPanel;
import de.oszimt.eImpfpass.view.PatientVaccineOverviewTableModel;

public class PatientVaccineOverviewController extends BaseController {

	private PatientVaccineOverviewPanel panel;
	private Patient patient;

	public PatientVaccineOverviewController(MainController controller, Patient patient) {
		super(controller);
		this.patient = patient;
	}

	@Override
	protected void initController() {
		// TODO Auto-generated method stub
		initView();
	}

	@Override
	protected void initView() {
		panel = new PatientVaccineOverviewPanel();
		try {
			List<PatientVaccine> patienten = controller.getModel().getPatientImpfung().getAll(patient.getId());
			panel.getTable().setModel(new PatientVaccineOverviewTableModel(patienten));
		} catch (SQLException e) {
			panel.setError(e.getLocalizedMessage());
			e.printStackTrace();
		}

		String hallo = "Impfübersicht für " + patient.getVorname() + " " + patient.getName();
		controller.getWindow().setActivePanel(hallo, panel);

		// add actions
		panel.getBackButton().addActionListener(e -> back());
		panel.getVacButton().addActionListener(e -> vacPatient());

	}

	@Override
	protected void initModel() {
		// TODO Auto-generated method stub

	}

	protected void back() {
		controller.setController(new PatientOverviewController(controller));
	}

	private void vacPatient() {
		controller.setController(new PatientVaccineEditController(controller, patient));
	}
}
