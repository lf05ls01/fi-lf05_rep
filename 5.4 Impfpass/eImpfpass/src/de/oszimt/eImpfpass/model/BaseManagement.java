package de.oszimt.eImpfpass.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseManagement<T> {
	protected final String primaryKey;
	protected final String table;
	protected MainModel modell;

	public BaseManagement(MainModel modell, String table, String id) {
		super();
		this.modell = modell;
		this.table = table;
		this.primaryKey = id;
	}

	protected int getLastID() throws SQLException {
		PreparedStatement query = null;
		try {
			String selectStatement = "SELECT " + primaryKey + " FROM " + table + " ORDER BY " + primaryKey
					+ " DESC LIMIT 1";
			query = modell.Connect(selectStatement);
			ResultSet rs = query.executeQuery();
			return rs.next() ? rs.getInt(primaryKey) : null;
		} finally {
			modell.Close(query);
		}
	}

	public T get(int id) throws SQLException {

		PreparedStatement query = null;
		try {
			String selectStatement = "SELECT * FROM " + table + " WHERE " + primaryKey + " = ?";
			query = modell.Connect(selectStatement);
			query.setInt(1, id);
			ResultSet rs = query.executeQuery();
			return rs.next() ? get(rs) : null;
		} finally {
			modell.Close(query);
		}
	}

	public T get(String name) throws SQLException {

		PreparedStatement query = null;
		try {
			String selectStatement = "SELECT * FROM " + table + " WHERE name = ?";
			query = modell.Connect(selectStatement);
			query.setString(1, name);
			ResultSet rs = query.executeQuery();
			return rs.next() ? get(rs) : null;
		} finally {
			modell.Close(query);
		}
	}

	public List<T> getAll() throws SQLException {
		ArrayList<T> all = new ArrayList<T>();
		PreparedStatement query = null;
		try {
			String selectStatement = "SELECT * FROM " + table;
			query = modell.Connect(selectStatement);
			ResultSet rs = query.executeQuery();

			while (rs.next()) {
				all.add(get(rs));
			}
			return all;
		} finally {
			modell.Close(query);
		}
	}

	protected abstract T get(ResultSet rs) throws SQLException;
}
