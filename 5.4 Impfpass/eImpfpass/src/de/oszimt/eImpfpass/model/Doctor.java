package de.oszimt.eImpfpass.model;

public class Doctor {
	private int aID;
	private String lastName;
	private String firstName;
	private String aFachgebiet;
	private String aStrasse;
	private int aPlz;
	private String town;

	public Doctor(int aID, String lastName, String firstName, String aFachgebiet, String aStrasse, int aPlz,
			String town) {
		this.aID = aID;
		this.lastName = lastName;
		this.firstName = firstName;
		this.aFachgebiet = aFachgebiet;
		this.aStrasse = aStrasse;
		this.aPlz = aPlz;
		this.town = town;
	}

	public int getaID() {
		return aID;
	}

	public void setaID(int aID) {
		this.aID = aID;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String aVorname) {
		this.firstName = aVorname;
	}

	public String getaFachgebiet() {
		return aFachgebiet;
	}

	public void setaFachgebiet(String aFachgebiet) {
		this.aFachgebiet = aFachgebiet;
	}

	public String getaStrasse() {
		return aStrasse;
	}

	public void setaStrasse(String aStrasse) {
		this.aStrasse = aStrasse;
	}

	public int getaPlz() {
		return aPlz;
	}

	public void setaPlz(int aPlz) {
		this.aPlz = aPlz;
	}

	public String getTown() {
		return town;
	}

	public void setTown(String aOrt) {
		this.town = aOrt;
	}

	public String getFullName() {
		return firstName + " " + lastName;
	}

}
