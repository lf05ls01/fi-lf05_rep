package de.oszimt.eImpfpass.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DoctorManagement extends BaseManagement<Doctor> {

	public DoctorManagement(MainModel modell) {
		super(modell, "t_aerzte", "aid");
	}

	@Override
	protected Doctor get(ResultSet rs) throws SQLException {
		return new Doctor(rs.getInt("aid"), rs.getString("aName"), rs.getString("aVorname"),
				rs.getString("aFachgebiet"), rs.getString("aStrasse"), rs.getInt("aPlz"), rs.getString("aOrt"));
	}

	/**
	 *
	 * @param id
	 * @param password
	 * @return Founded Doctor or null
	 */
	public Doctor login(int id, String password) {
		// TODO add password
		// TODO password hash
		try {
			return get(id);
		} catch (SQLException throwables) {
			return null;
		}
	}

	public String register(int id, String password) throws Exception {
		// TODO register
		throw new Exception("Not implemented");
	}
}
