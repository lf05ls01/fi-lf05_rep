package de.oszimt.eImpfpass.model;

@Deprecated
public class Erreger {
	private int eID;
	private String eName;
	private String eKrankheit;

	public Erreger(int eID, String eName, String eKrankheit) {
		this.eID = eID;
		this.eName = eName;
		this.eKrankheit = eKrankheit;
	}

	public int geteID() {
		return eID;
	}

	public void seteID(int eID) {
		this.eID = eID;
	}

	public String geteName() {
		return eName;
	}

	public void seteName(String eName) {
		this.eName = eName;
	}

	public String geteKrankheit() {
		return eKrankheit;
	}

	public void seteKrankheit(String eKrankheit) {
		this.eKrankheit = eKrankheit;
	}

}
