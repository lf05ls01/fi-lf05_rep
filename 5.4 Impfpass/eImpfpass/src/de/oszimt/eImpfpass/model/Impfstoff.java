package de.oszimt.eImpfpass.model;

public class Impfstoff {
	private int isID;
	private String isBezeichnung;
	private String isHersteller;
	private boolean isKombi;
	private int iID;

	public Impfstoff(int isID, String isBezeichnung, String isHersteller, boolean isKombi, int iID) {
		this.isID = isID;
		this.isBezeichnung = isBezeichnung;
		this.isHersteller = isHersteller;
		this.isKombi = isKombi;
		this.iID = iID;
	}

	public int getIsID() {
		return isID;
	}

	public void setIsID(int isID) {
		this.isID = isID;
	}

	public String getIsBezeichnung() {
		return isBezeichnung;
	}

	public void setIsBezeichnung(String isBezeichnung) {
		this.isBezeichnung = isBezeichnung;
	}

	public String getIsHersteller() {
		return isHersteller;
	}

	public void setIsHersteller(String isHersteller) {
		this.isHersteller = isHersteller;
	}

	public boolean isKombi() {
		return isKombi;
	}

	public void setKombi(boolean isKombi) {
		this.isKombi = isKombi;
	}

	public int getiID() {
		return iID;
	}

	public void setiID(int iID) {
		this.iID = iID;
	}

}
