package de.oszimt.eImpfpass.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class MainModel {

	private static final String dburl = "jdbc:mysql://localhost/eimpfpass?serverTimezone=Europe/Berlin";
	private static final String userid = "root";
	private static final String pwd = "";

	private Connection con;

	private PatientManagement patient;
	private PatientVaccineManagement patientImpfung;
	private DoctorManagement arzt;
	private ManagementVaccine impfung;

	public void init() {
		patient = new PatientManagement(this);
		patientImpfung = new PatientVaccineManagement(this);
		arzt = new DoctorManagement(this);
		impfung = new ManagementVaccine(this);
	}

	public PreparedStatement Connect(String query) throws SQLException {
		con = DriverManager.getConnection(dburl, userid, pwd);
		PreparedStatement stmt = con.prepareStatement(query);
		return stmt;
	}

	public void Close(PreparedStatement stmt) throws SQLException {
		if (stmt != null) {
			stmt.close();
		}
		if (con != null) {
			con.close();
		}
	}

	/**
	 * @return the patient
	 */
	public PatientManagement getPatient() {
		return patient;
	}

	/**
	 * @return the arzt
	 */
	public DoctorManagement getArzt() {
		return arzt;
	}

	public ManagementVaccine getImpfung() {
		return impfung;
	}

	public PatientVaccineManagement getPatientImpfung() {
		return patientImpfung;
	}

}
