package de.oszimt.eImpfpass.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ManagementVaccine extends BaseManagement<Vaccine> {

	public ManagementVaccine(MainModel modell) {
		super(modell, "t_impfungen", "iid");
	}

	protected Vaccine get(ResultSet rs) throws SQLException {
		return new Vaccine(rs.getInt("iID"), rs.getString("name"), rs.getInt("gueltigJahre"));
	}

	public List<Vaccine> getAllNotPatient(int pid) throws SQLException {
		List<Vaccine> all = new ArrayList<>();

		PreparedStatement query = null;
		try {
			String selectStatement = "SELECT * FROM t_impfungen WHERE `iid` NOT IN(SELECT iid FROM t_patienten_impfungen WHERE `pid` = ?)";
			query = modell.Connect(selectStatement);
			query.setInt(1, pid);
			ResultSet rs = query.executeQuery();

			while (rs.next()) {
				all.add(get(rs));
			}

			return all;
		} finally {
			modell.Close(query);
		}
	}
}