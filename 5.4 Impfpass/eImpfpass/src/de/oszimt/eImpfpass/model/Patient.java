package de.oszimt.eImpfpass.model;

import java.time.LocalDate;

public class Patient {
	private int id;
	private String name;
	private String vorname;
	private LocalDate birthday;
	private String street;
	private int plz;
	private String town;

	public Patient(int id, String name, String vorname, LocalDate birthday, String street, int plz, String town) {
		super();
		this.id = id;
		this.name = name;
		this.vorname = vorname;
		this.birthday = birthday;
		this.street = street;
		this.plz = plz;
		this.town = town;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public LocalDate getBirthday() {
		return birthday;
	}

	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public int getPlz() {
		return plz;
	}

	public void setPlz(int plz) {
		this.plz = plz;
	}

	public String getTown() {
		return town;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public String getFullName() {
		return vorname + " " + name;
	}
}
