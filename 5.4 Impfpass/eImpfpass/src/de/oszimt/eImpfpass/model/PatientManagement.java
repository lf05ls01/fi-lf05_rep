/**
 *
 */
package de.oszimt.eImpfpass.model;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

/**
 * @author sven
 *
 */
public class PatientManagement extends BaseManagement<Patient> {

	public PatientManagement(MainModel modell) {
		super(modell, "t_patienten", "pid");
	}

	public Patient create(String name, String vName, LocalDate gebDat, String address, int plz, String ort)
			throws SQLException {
		PreparedStatement query = null;
		try {

			final String selectStatement = "INSERT INTO `t_patienten` VALUES(?, ?, ?, ?, ?, ?, ?)";
			query = modell.Connect(selectStatement);
			query.setInt(1, getLastID() + 1);
			query.setString(2, name);
			query.setString(2, name);
			query.setString(3, vName);
			query.setDate(4, Date.valueOf(gebDat));
			query.setString(5, address);
			query.setInt(6, plz);
			query.setString(7, ort);
			query.executeUpdate();

			return get(getLastID());
		} finally {
			modell.Close(query);
		}
	}

	@Override
	public Patient get(ResultSet rs) throws SQLException {
		return new Patient(rs.getInt("PID"), rs.getString("name"), rs.getString("vorname"),
				rs.getDate("birthday").toLocalDate(), rs.getString("street"), rs.getInt("plz"), rs.getString("town"));
	}

	public void update(Patient patient) throws SQLException {
		PreparedStatement query = null;
		try {
			final String selectStatement = "UPDATE `t_patienten` SET name = ?, vorname = ?, birthday = ?, street = ?, plz = ?, town = ?  WHERE pid = ?";
			query = modell.Connect(selectStatement);
			query.setString(1, patient.getName());
			query.setString(2, patient.getVorname());
			query.setDate(3, Date.valueOf(patient.getBirthday()));
			query.setString(4, patient.getStreet());
			query.setInt(5, patient.getPlz());
			query.setString(6, patient.getTown());
			query.setInt(7, patient.getId());
			query.executeUpdate();
		} finally {
			modell.Close(query);
		}
	}
}
