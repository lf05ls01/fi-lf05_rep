package de.oszimt.eImpfpass.model;

import java.time.LocalDate;

public class PatientVaccine {

	private int pid;
	private int iid;
	private int aid;
	private LocalDate date;
	private String name;
	private int gueltigJahre;

	public PatientVaccine(int pid, int iid, int aid, LocalDate date, String name, int gueltigJahre) {
		super();
		this.pid = pid;
		this.iid = iid;
		this.aid = aid;
		this.date = date;
		this.name = name;
		this.gueltigJahre = gueltigJahre;
	}

	public int getPid() {
		return pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public int getIid() {
		return iid;
	}

	public void setIid(int iid) {
		this.iid = iid;
	}

	public int getAid() {
		return aid;
	}

	public void setAid(int aid) {
		this.aid = aid;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public String getName() {
		return name;
	}

	public int getGueltigJahre() {
		return gueltigJahre;
	}

}
