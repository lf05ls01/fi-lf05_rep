package de.oszimt.eImpfpass.model;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class PatientVaccineManagement extends BaseManagement<PatientVaccine> {

	public PatientVaccineManagement(MainModel modell) {
		super(modell, "t_patienten_impfungen", "pid");
	}

	@Override
	protected PatientVaccine get(ResultSet rs) throws SQLException {
		try {
			return new PatientVaccine(rs.getInt("pid"), rs.getInt("iid"), rs.getInt("aid"),
					rs.getDate("date").toLocalDate(), rs.getString("name"), rs.getInt("gueltigjahre"));
		} catch (SQLException e) {
			return new PatientVaccine(rs.getInt("pid"), rs.getInt("iid"), rs.getInt("aid"),
					rs.getDate("date").toLocalDate(), null, 0);
		}
	}

	public List<PatientVaccine> getAll(int pid) throws SQLException {
		List<PatientVaccine> all = new ArrayList<>();

		String selectStatement = "SELECT * FROM t_impfungen JOIN t_patienten_impfungen ON t_impfungen.iid = t_patienten_impfungen.iid WHERE pid = ?";
		PreparedStatement query = modell.Connect(selectStatement);
		query.setInt(1, pid);
		ResultSet rs = query.executeQuery();

		while (rs.next()) {
			all.add(get(rs));
		}

		modell.Close(query);

		return all;
	}

	public void Create(int pid, int iid, int aid, LocalDate date) throws SQLException {

		PreparedStatement query = null;
		try {

			String selectStatement = "INSERT INTO t_patienten_impfungen VALUES(?, ?, ?, ?)";
			query = modell.Connect(selectStatement);
			query.setInt(1, pid);
			query.setInt(2, iid);
			query.setInt(3, aid);
			query.setDate(4, Date.valueOf(date));
			query.executeUpdate();
		} finally {
			modell.Close(query);
		}
	}
}