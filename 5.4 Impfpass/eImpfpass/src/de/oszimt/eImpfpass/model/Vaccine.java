package de.oszimt.eImpfpass.model;

public class Vaccine {
	private final int iID;
	private final String name;
	private final int gueltigJahre;

	public Vaccine(int iID, String name, int gueltigJahre) {
		this.iID = iID;
		this.name = name;
		this.gueltigJahre = gueltigJahre;
	}

	public int getiID() {
		return iID;
	}

	public String getName() {
		return name;
	}

	public int getGueltigJahre() {
		return gueltigJahre;
	}

	@Override
	public String toString() {
		return "Impfung " + name;
	}

}
