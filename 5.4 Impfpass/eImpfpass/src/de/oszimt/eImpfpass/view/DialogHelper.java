/**
 *
 */
package de.oszimt.eImpfpass.view;

import java.awt.EventQueue;

import javax.swing.JOptionPane;

/**
 * @author sven
 *
 */
public class DialogHelper {

	public static void displayError(Exception e) {
		displayError(e.getClass() + " " + e.getLocalizedMessage());
	}

	public static void displayError(String e) {
		EventQueue.invokeLater(() -> {
			try {
				JOptionPane.showMessageDialog(null, e, "An error happend", JOptionPane.ERROR_MESSAGE);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		});
	}
}
