package de.oszimt.eImpfpass.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class LoginPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4411327355187092337L;
	private JTextField loginName;
	private JPasswordField loginPassword;
	private JButton loginButton;
	private JButton registerButton;
	private JPanel buttons;
	private JPanel login;
	private JLabel lblError;

	/**
	 * Create the panel.
	 */
	public LoginPanel() {
		setLayout(new BorderLayout(0, 0));

		login = new JPanel();
		add(login, BorderLayout.CENTER);
		GridBagLayout gbl_loginq = new GridBagLayout();
		gbl_loginq.columnWidths = new int[] { 36, 316, 0 };
		gbl_loginq.rowHeights = new int[] { 26, 0, 0, 0 };
		gbl_loginq.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		gbl_loginq.rowWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
		login.setLayout(gbl_loginq);

		JLabel lblName = new JLabel("ID");
		GridBagConstraints gbc_lblName = new GridBagConstraints();
		gbc_lblName.anchor = GridBagConstraints.EAST;
		gbc_lblName.insets = new Insets(0, 0, 5, 5);
		gbc_lblName.gridx = 0;
		gbc_lblName.gridy = 0;
		login.add(lblName, gbc_lblName);

		loginPassword = new JPasswordField();
		loginPassword.setColumns(10);
		GridBagConstraints gbc_loginPassword = new GridBagConstraints();
		gbc_loginPassword.fill = GridBagConstraints.HORIZONTAL;
		gbc_loginPassword.insets = new Insets(0, 0, 5, 0);
		gbc_loginPassword.anchor = GridBagConstraints.NORTH;
		gbc_loginPassword.gridx = 1;
		gbc_loginPassword.gridy = 1;
		login.add(loginPassword, gbc_loginPassword);

		JLabel lblPassword = new JLabel("Password");
		GridBagConstraints gbc_lblPassword = new GridBagConstraints();
		gbc_lblPassword.anchor = GridBagConstraints.EAST;
		gbc_lblPassword.insets = new Insets(0, 0, 5, 5);
		gbc_lblPassword.gridx = 0;
		gbc_lblPassword.gridy = 1;
		login.add(lblPassword, gbc_lblPassword);

		loginName = new JTextField();
		loginName.setColumns(10);
		GridBagConstraints gbc_loginName = new GridBagConstraints();
		gbc_loginName.insets = new Insets(0, 0, 5, 0);
		gbc_loginName.fill = GridBagConstraints.HORIZONTAL;
		gbc_loginName.anchor = GridBagConstraints.NORTH;
		gbc_loginName.gridx = 1;
		gbc_loginName.gridy = 0;
		login.add(loginName, gbc_loginName);

		lblError = new JLabel("");
		lblError.setForeground(new Color(165, 42, 42));
		GridBagConstraints gbc_lblError = new GridBagConstraints();
		gbc_lblError.anchor = GridBagConstraints.WEST;
		gbc_lblError.gridx = 1;
		gbc_lblError.gridy = 2;
		login.add(lblError, gbc_lblError);

		buttons = new JPanel();
		add(buttons, BorderLayout.SOUTH);

		registerButton = new JButton("Register");
		buttons.add(registerButton);

		loginButton = new JButton("Login");
		buttons.add(loginButton);

	}

	public JTextField getLoginName() {
		return loginName;
	}

	public JTextField getLoginPassword() {
		return loginPassword;
	}

	public JButton getLoginButton() {
		return loginButton;
	}

	public JButton getRegisterButton() {
		return registerButton;
	}

	public void setError(String text) {
		lblError.setForeground(new Color(165, 42, 42));
		lblError.setText(text);
	}

	public void setMessage(String text) {
		lblError.setForeground(new Color(0, 128, 0));
		lblError.setText(text);
	}
}
