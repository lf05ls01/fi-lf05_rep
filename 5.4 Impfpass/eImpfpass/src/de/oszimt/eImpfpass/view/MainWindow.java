package de.oszimt.eImpfpass.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.SystemColor;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class MainWindow extends JFrame {

	private static final long serialVersionUID = -993915770688673843L;
	private JPanel contentPane;
	private JLabel lblHeader;

	/**
	 * Launch the application.
	 */
	public void init() {
		setVisible(true);
	}

	/**
	 * Create the frame.
	 */
	public MainWindow() {
		setTitle("eImpfpass");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(500, 600);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(135, 206, 235));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		lblHeader = new JLabel("Header");
		lblHeader.setFont(new Font("Lucida Grande", Font.PLAIN, 20));
		lblHeader.setHorizontalAlignment(SwingConstants.CENTER);
		lblHeader.setBackground(SystemColor.windowBorder);
		contentPane.add(lblHeader, BorderLayout.NORTH);
	}

	public void setActivePanel(String header, JPanel panel) {
		lblHeader.setText(header);
		lblHeader.invalidate();
		if (contentPane.getComponentCount() >= 2)
			contentPane.remove(1);
		contentPane.invalidate();
		contentPane.add(panel, BorderLayout.CENTER);

	}

}
