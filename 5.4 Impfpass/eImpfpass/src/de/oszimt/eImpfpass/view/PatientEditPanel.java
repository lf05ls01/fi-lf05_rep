package de.oszimt.eImpfpass.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

public class PatientEditPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4411327355187092337L;
	private JTextField id;

	private JTextField vorname;
	private JButton saveButton;

	private JButton backButton;
	private JPanel buttons;
	private JPanel login;
	private JLabel lblError;
	private JTextField lastName;
	private JTextField street;
	private JTextField birthday;
	private JTextField town;
	private JScrollPane scrollPane;

	/**
	 * Create the panel.
	 */
	public PatientEditPanel() {
		setLayout(new BorderLayout(0, 0));

		scrollPane = new JScrollPane();
		add(scrollPane, BorderLayout.CENTER);

		login = new JPanel();
		GridBagLayout gbl_loginq = new GridBagLayout();
		gbl_loginq.columnWidths = new int[] { 36, 316, 0 };
		gbl_loginq.rowHeights = new int[] { 26, 0, 0, 0, 0, 0, 0, 0 };
		gbl_loginq.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		gbl_loginq.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		login.setLayout(gbl_loginq);
		scrollPane.setViewportView(login);

		JLabel lblName = new JLabel("ID");
		GridBagConstraints gbc_lblName = new GridBagConstraints();
		gbc_lblName.anchor = GridBagConstraints.EAST;
		gbc_lblName.insets = new Insets(0, 0, 5, 5);
		gbc_lblName.gridx = 0;
		gbc_lblName.gridy = 0;
		login.add(lblName, gbc_lblName);

		id = new JTextField();
		GridBagConstraints gbc_id = new GridBagConstraints();
		gbc_id.fill = GridBagConstraints.HORIZONTAL;
		gbc_id.insets = new Insets(0, 0, 5, 0);
		gbc_id.gridx = 1;
		gbc_id.gridy = 0;
		login.add(id, gbc_id);
		id.setEditable(false);
		id.setColumns(10);

		vorname = new JTextField();
		GridBagConstraints gbc_vorname = new GridBagConstraints();
		gbc_vorname.fill = GridBagConstraints.HORIZONTAL;
		gbc_vorname.insets = new Insets(0, 0, 5, 0);
		gbc_vorname.anchor = GridBagConstraints.NORTH;
		gbc_vorname.gridx = 1;
		gbc_vorname.gridy = 1;
		login.add(vorname, gbc_vorname);

		JLabel lblVorname = new JLabel("Vorname");
		GridBagConstraints gbc_lblVorname = new GridBagConstraints();
		gbc_lblVorname.anchor = GridBagConstraints.EAST;
		gbc_lblVorname.insets = new Insets(0, 0, 5, 5);
		gbc_lblVorname.gridx = 0;
		gbc_lblVorname.gridy = 1;
		login.add(lblVorname, gbc_lblVorname);

		JLabel lblNachname = new JLabel("Nachname");
		GridBagConstraints gbc_lblNachname = new GridBagConstraints();
		gbc_lblNachname.anchor = GridBagConstraints.EAST;
		gbc_lblNachname.insets = new Insets(0, 0, 5, 5);
		gbc_lblNachname.gridx = 0;
		gbc_lblNachname.gridy = 2;
		login.add(lblNachname, gbc_lblNachname);

		lastName = new JTextField();
		GridBagConstraints gbc_name = new GridBagConstraints();
		gbc_name.insets = new Insets(0, 0, 5, 0);
		gbc_name.fill = GridBagConstraints.HORIZONTAL;
		gbc_name.gridx = 1;
		gbc_name.gridy = 2;
		login.add(lastName, gbc_name);
		lastName.setColumns(10);

		JLabel lblGeb = new JLabel("Geburtsdatum");
		GridBagConstraints gbc_lblGeb = new GridBagConstraints();
		gbc_lblGeb.insets = new Insets(0, 0, 5, 5);
		gbc_lblGeb.anchor = GridBagConstraints.EAST;
		gbc_lblGeb.gridx = 0;
		gbc_lblGeb.gridy = 3;
		login.add(lblGeb, gbc_lblGeb);

		birthday = new JTextField();
		GridBagConstraints gbc_birthday = new GridBagConstraints();
		gbc_birthday.insets = new Insets(0, 0, 5, 0);
		gbc_birthday.fill = GridBagConstraints.HORIZONTAL;
		gbc_birthday.gridx = 1;
		gbc_birthday.gridy = 3;
		login.add(birthday, gbc_birthday);
		birthday.setColumns(10);

		JLabel lblStreet = new JLabel("Straße");
		GridBagConstraints gbc_lblStreet = new GridBagConstraints();
		gbc_lblStreet.anchor = GridBagConstraints.EAST;
		gbc_lblStreet.insets = new Insets(0, 0, 5, 5);
		gbc_lblStreet.gridx = 0;
		gbc_lblStreet.gridy = 4;
		login.add(lblStreet, gbc_lblStreet);

		street = new JTextField();
		GridBagConstraints gbc_street = new GridBagConstraints();
		gbc_street.insets = new Insets(0, 0, 5, 0);
		gbc_street.fill = GridBagConstraints.HORIZONTAL;
		gbc_street.gridx = 1;
		gbc_street.gridy = 4;
		login.add(street, gbc_street);
		street.setColumns(10);

		JLabel LblTown = new JLabel("Town");
		GridBagConstraints gbc_LblTown = new GridBagConstraints();
		gbc_LblTown.anchor = GridBagConstraints.EAST;
		gbc_LblTown.insets = new Insets(0, 0, 5, 5);
		gbc_LblTown.gridx = 0;
		gbc_LblTown.gridy = 5;
		login.add(LblTown, gbc_LblTown);

		town = new JTextField();
		GridBagConstraints gbc_town = new GridBagConstraints();
		gbc_town.insets = new Insets(0, 0, 5, 0);
		gbc_town.fill = GridBagConstraints.HORIZONTAL;
		gbc_town.gridx = 1;
		gbc_town.gridy = 5;
		login.add(town, gbc_town);
		town.setColumns(10);

		lblError = new JLabel("  ");
		lblError.setForeground(new Color(165, 42, 42));
		GridBagConstraints gbc_lblError = new GridBagConstraints();
		gbc_lblError.anchor = GridBagConstraints.WEST;
		gbc_lblError.gridx = 1;
		gbc_lblError.gridy = 6;
		login.add(lblError, gbc_lblError);

		buttons = new JPanel();
		add(buttons, BorderLayout.SOUTH);

		backButton = new JButton("Zurück");
		buttons.add(backButton);

		saveButton = new JButton("Speichern");
		buttons.add(saveButton);

	}

	public void setError(String text) {
		lblError.setForeground(new Color(165, 42, 42));
		lblError.setText(text);
	}

	public void setMessage(String text) {
		lblError.setForeground(new Color(0, 128, 0));
		lblError.setText(text);
	}

	public JTextField getVorname() {
		return vorname;
	}

	public JTextField getLastName() {
		return lastName;
	}

	public JTextField getStreet() {
		return street;
	}

	public JTextField getBirthday() {
		return birthday;
	}

	public JTextField getTown() {
		return town;
	}

	public JButton getSaveButton() {
		return saveButton;
	}

	public JButton getBackButton() {
		return backButton;
	}

	public JTextField getId() {
		return id;
	}
}
