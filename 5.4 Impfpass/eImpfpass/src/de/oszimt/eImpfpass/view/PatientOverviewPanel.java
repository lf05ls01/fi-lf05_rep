package de.oszimt.eImpfpass.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;

public class PatientOverviewPanel extends JPanel {
	private static final long serialVersionUID = 3125578517548473536L;

	private JLabel lblMess;
	private JTable table;
	private JButton vacButton;
	private JButton editButton;
	private JButton newButton;
	private JButton logoutButton;
	private Component horizontalGlue;

	public JTable getTable() {
		return table;
	}

	/**
	 * Create the panel.
	 */
	public PatientOverviewPanel() {
		setLayout(new BorderLayout(0, 0));

		JToolBar toolBar = new JToolBar();
		add(toolBar, BorderLayout.NORTH);

		newButton = new JButton("Neuer Patient");
		toolBar.add(newButton);

		editButton = new JButton("Ändere Patient");
		// editButton.setEnabled(false);
		toolBar.add(editButton);

		vacButton = new JButton("Impfe Patient");
		// vacButton.setEnabled(false);
		toolBar.add(vacButton);

		horizontalGlue = Box.createHorizontalGlue();
		toolBar.add(horizontalGlue);

		logoutButton = new JButton("Abmelden");
		toolBar.add(logoutButton);

		lblMess = new JLabel("");
		add(lblMess, BorderLayout.SOUTH);

		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane, BorderLayout.CENTER);

		table = new JTable();
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(table);

	}

	public void setError(String text) {
		lblMess.setForeground(new Color(165, 42, 42));
		lblMess.setText(text);
	}

	public void setMessage(String text) {
		lblMess.setForeground(new Color(0, 128, 0));
		lblMess.setText(text);
	}

	public JButton getEditButton() {
		return editButton;
	}

	public JButton getVacButton() {
		return vacButton;
	}

	public JButton getNewButton() {
		return newButton;
	}

	public JButton getLogoutButton() {
		return logoutButton;
	}

}
