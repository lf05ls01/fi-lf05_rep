package de.oszimt.eImpfpass.view;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import de.oszimt.eImpfpass.model.Patient;

public class PatientOverviewTableModel extends AbstractTableModel {

	private static final long serialVersionUID = -4320131363442527148L;

	String[] columnNames = { "ID", "Nachname", "Vorname", "Geburtsdatum" };

	private final List<Patient> patienten;

	public PatientOverviewTableModel(List<Patient> patienten) {
		this.patienten = patienten;
	}

	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}

	@Override
	public int getRowCount() {
		return patienten.size();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		final Patient t = patienten.get(rowIndex);

		if (columnIndex == 0) {
			return t.getId();
		}
		if (columnIndex == 1) {
			return t.getName();
		}
		if (columnIndex == 2) {
			return t.getVorname();
		}
		if (columnIndex == 3) {
			return t.getBirthday();
		}

		return "?";
	}

//	/*
//     * Don't need to implement this method unless your table's
//     * editable.
//     */
//    public boolean isCellEditable(int row, int col) {
//        //Note that the data/cell address is constant,
//        //no matter where the cell appears onscreen.
//        if (col < 2) {
//            return false;
//        } else {
//            return true;
//        }
//    }
//
//    /*
//     * Don't need to implement this method unless your table's
//     * data can change.
//     */
//    public void setValueAt(Object value, int row, int col) {
//        data[row][col] = value;
//        fireTableCellUpdated(row, col);
//    }

}
