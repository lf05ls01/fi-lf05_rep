package de.oszimt.eImpfpass.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import de.oszimt.eImpfpass.model.Vaccine;

public class PatientVaccineEditPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4411327355187092337L;
	private JTextField patient;

	private JTextField doctor;
	private JButton saveButton;
	private JButton backButton;
	private JPanel buttons;
	private JPanel login;
	private JLabel lblError;
	private JTextField date;
	private JScrollPane scrollPane;
	private JComboBox<Vaccine> vac;

	/**
	 * Create the panel.
	 */
	public PatientVaccineEditPanel() {
		setLayout(new BorderLayout(0, 0));

		scrollPane = new JScrollPane();
		add(scrollPane, BorderLayout.CENTER);

		login = new JPanel();
		GridBagLayout gbl_loginq = new GridBagLayout();
		gbl_loginq.columnWidths = new int[] { 36, 316, 0 };
		gbl_loginq.rowHeights = new int[] { 26, 0, 0, 0, 0, 0 };
		gbl_loginq.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		gbl_loginq.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		login.setLayout(gbl_loginq);
		scrollPane.setViewportView(login);

		JLabel lblPatient = new JLabel("Patient");
		GridBagConstraints gbc_lblPatient = new GridBagConstraints();
		gbc_lblPatient.anchor = GridBagConstraints.EAST;
		gbc_lblPatient.insets = new Insets(0, 0, 5, 5);
		gbc_lblPatient.gridx = 0;
		gbc_lblPatient.gridy = 0;
		login.add(lblPatient, gbc_lblPatient);

		patient = new JTextField();
		GridBagConstraints gbc_patient = new GridBagConstraints();
		gbc_patient.fill = GridBagConstraints.HORIZONTAL;
		gbc_patient.insets = new Insets(0, 0, 5, 0);
		gbc_patient.gridx = 1;
		gbc_patient.gridy = 0;
		login.add(patient, gbc_patient);
		patient.setEditable(false);
		patient.setColumns(10);

		doctor = new JTextField();
		doctor.setEditable(false);
		GridBagConstraints gbc_doctor = new GridBagConstraints();
		gbc_doctor.fill = GridBagConstraints.HORIZONTAL;
		gbc_doctor.insets = new Insets(0, 0, 5, 0);
		gbc_doctor.anchor = GridBagConstraints.NORTH;
		gbc_doctor.gridx = 1;
		gbc_doctor.gridy = 1;
		login.add(doctor, gbc_doctor);

		JLabel lblArzt = new JLabel("Arzt");
		GridBagConstraints gbc_lblArzt = new GridBagConstraints();
		gbc_lblArzt.anchor = GridBagConstraints.EAST;
		gbc_lblArzt.insets = new Insets(0, 0, 5, 5);
		gbc_lblArzt.gridx = 0;
		gbc_lblArzt.gridy = 1;
		login.add(lblArzt, gbc_lblArzt);

		JLabel lblImpfung = new JLabel("Impfung");
		GridBagConstraints gbc_lblImpfung = new GridBagConstraints();
		gbc_lblImpfung.anchor = GridBagConstraints.EAST;
		gbc_lblImpfung.insets = new Insets(0, 0, 5, 5);
		gbc_lblImpfung.gridx = 0;
		gbc_lblImpfung.gridy = 2;
		login.add(lblImpfung, gbc_lblImpfung);

		vac = new JComboBox<Vaccine>();
		GridBagConstraints gbc_vac = new GridBagConstraints();
		gbc_vac.insets = new Insets(0, 0, 5, 0);
		gbc_vac.fill = GridBagConstraints.HORIZONTAL;
		gbc_vac.gridx = 1;
		gbc_vac.gridy = 2;
		login.add(vac, gbc_vac);

		JLabel lblDate = new JLabel("Datum");
		GridBagConstraints gbc_lblDate = new GridBagConstraints();
		gbc_lblDate.insets = new Insets(0, 0, 5, 5);
		gbc_lblDate.anchor = GridBagConstraints.EAST;
		gbc_lblDate.gridx = 0;
		gbc_lblDate.gridy = 3;
		login.add(lblDate, gbc_lblDate);

		date = new JTextField();
		GridBagConstraints gbc_date = new GridBagConstraints();
		gbc_date.insets = new Insets(0, 0, 5, 0);
		gbc_date.fill = GridBagConstraints.HORIZONTAL;
		gbc_date.gridx = 1;
		gbc_date.gridy = 3;
		login.add(date, gbc_date);
		date.setColumns(10);

		lblError = new JLabel("  ");
		lblError.setForeground(new Color(165, 42, 42));
		GridBagConstraints gbc_lblError = new GridBagConstraints();
		gbc_lblError.anchor = GridBagConstraints.WEST;
		gbc_lblError.gridx = 1;
		gbc_lblError.gridy = 4;
		login.add(lblError, gbc_lblError);

		buttons = new JPanel();
		add(buttons, BorderLayout.SOUTH);

		backButton = new JButton("Zurück");
		buttons.add(backButton);

		saveButton = new JButton("Impfen");
		buttons.add(saveButton);

	}

	public void setError(String text) {
		lblError.setForeground(new Color(165, 42, 42));
		lblError.setText(text);
	}

	public void setMessage(String text) {
		lblError.setForeground(new Color(0, 128, 0));
		lblError.setText(text);
	}

	public JTextField getDoctor() {
		return doctor;
	}

	public JComboBox<Vaccine> getVac() {
		return vac;
	}

	public JTextField getDate() {
		return date;
	}

	public JButton getSaveButton() {
		return saveButton;
	}

	public JButton getBackButton() {
		return backButton;
	}

	public JTextField getPatient() {
		return patient;
	}
}
