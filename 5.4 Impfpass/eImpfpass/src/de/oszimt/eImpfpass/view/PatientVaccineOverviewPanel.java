package de.oszimt.eImpfpass.view;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;

public class PatientVaccineOverviewPanel extends JPanel {
	private static final long serialVersionUID = 3125578517548473536L;

	private JLabel lblMess;
	private JTable table;
	private JButton vacButton;
	private JButton backButton;

	public JTable getTable() {
		return table;
	}

	/**
	 * Create the panel.
	 */
	public PatientVaccineOverviewPanel() {
		setLayout(new BorderLayout(0, 0));

		JToolBar toolBar = new JToolBar();
		add(toolBar, BorderLayout.NORTH);

		backButton = new JButton("Zurück");
		toolBar.add(backButton);

		vacButton = new JButton("Impfe Patient");
		toolBar.add(vacButton);

		lblMess = new JLabel("");
		add(lblMess, BorderLayout.SOUTH);

		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane, BorderLayout.CENTER);

		table = new JTable();
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(table);

	}

	public void setError(String text) {
		lblMess.setForeground(new Color(165, 42, 42));
		lblMess.setText(text);
	}

	public void setMessage(String text) {
		lblMess.setForeground(new Color(0, 128, 0));
		lblMess.setText(text);
	}

	public JButton getVacButton() {
		return vacButton;
	}

	public JButton getBackButton() {
		return backButton;
	}

}
