package de.oszimt.eImpfpass.view;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import de.oszimt.eImpfpass.model.PatientVaccine;

public class PatientVaccineOverviewTableModel extends AbstractTableModel {

	private static final long serialVersionUID = -4320131363442527148L;

	String[] columnNames = { "Nmae", "Datum", "Gültig in Jahren" };

	private final List<PatientVaccine> impfung;

	public PatientVaccineOverviewTableModel(List<PatientVaccine> impfung) {
		this.impfung = impfung;
	}

	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}

	@Override
	public int getRowCount() {
		return impfung.size();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		final PatientVaccine t = impfung.get(rowIndex);

		if (columnIndex == 0) {
			return t.getName();
		}
		if (columnIndex == 1) {
			return t.getDate();
		}
		if (columnIndex == 2) {
			return t.getGueltigJahre();
		}

		return "?";
	}

//	/*
//     * Don't need to implement this method unless your table's
//     * editable.
//     */
//    public boolean isCellEditable(int row, int col) {
//        //Note that the data/cell address is constant,
//        //no matter where the cell appears onscreen.
//        if (col < 2) {
//            return false;
//        } else {
//            return true;
//        }
//    }
//
//    /*
//     * Don't need to implement this method unless your table's
//     * data can change.
//     */
//    public void setValueAt(Object value, int row, int col) {
//        data[row][col] = value;
//        fireTableCellUpdated(row, col);
//    }

}
