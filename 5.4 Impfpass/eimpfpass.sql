-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 19, 2020 at 09:31 PM
-- Server version: 5.6.35
-- PHP Version: 7.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eimpfpass`
--
CREATE DATABASE IF NOT EXISTS `eimpfpass` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `eimpfpass`;

-- --------------------------------------------------------

--
-- Table structure for table `T_Aerzte`
--

DROP TABLE IF EXISTS `T_Aerzte`;
CREATE TABLE `T_Aerzte` (
  `aid` int(4) NOT NULL,
  `aname` varchar(30) NOT NULL,
  `avorname` varchar(20) DEFAULT NULL,
  `afachgebiet` varchar(20) NOT NULL,
  `astrasse` varchar(30) DEFAULT NULL,
  `aplz` int(5) DEFAULT NULL,
  `aort` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `T_Aerzte`
--

INSERT INTO `T_Aerzte` (`aid`, `aname`, `avorname`, `afachgebiet`, `astrasse`, `aplz`, `aort`) VALUES
(3001, 'von Behring', 'Emil', 'Immunologie', 'Biegenstr. 51', 35037, 'Marburg'),
(3002, 'Fleming', 'Alexander', 'Immunologie', 'Urbanstr. 169', 10961, 'Berlin'),
(3003, 'Koch', 'Robert', 'Immunologie', 'Gernsbacher Str. 2', 76530, 'Baden-Baden'),
(3004, 'Pasteur', 'Louis', 'Immunologie', 'Ferdinandstr. 35', 12209, 'Berlin'),
(3005, 'Wahnsinn', 'Hella', 'Kinderheilkunde', 'Karlsruher Str. 7a', 10711, 'Berlin'),
(3006, 'Sehen', 'Ann', 'Augenheilkunde', 'Podbielskiallee 77', 14195, 'Berlin'),
(3007, 'Rensolo', 'Gitta', 'HNO', 'Kladower Damm 366', 14089, 'Berlin'),
(3008, 'Schmund', 'Knuth', 'Zahnmedizin', 'Suhler Str. 35-37', 12629, 'Berlin'),
(3009, 'Fall', 'Klara', 'Allgemeinmedizin', 'Wilmersdorfer Str. 42', 10627, 'Berlin'),
(3010, 'Utzmich', 'Ben', 'Allgemeinmedizin', 'Assmannshauser Str. 10a', 14197, 'Berlin'),
(3011, 'Trophobie', 'Klaus', 'Kinderheilkunde', 'Reichsstr. 103', 14052, 'Berlin'),
(3012, 'Uebel', 'Izmir', 'Innere Medizin', 'Leistikowstr. 2', 14050, 'Berlin'),
(3013, 'Bolika', 'Anna', 'Sportmedizin', 'Schlossstr. 26', 12163, 'Berlin'),
(3014, 'Derbuch', 'Bill', 'Kinderheilkunde', 'Bundesplatz 14', 10715, 'Berlin'),
(3015, 'Paede', 'Otto', 'Orthopaedie', 'Joachim-Friedrich-Str. 16', 10711, 'Berlin'),
(3016, 'Telang', 'Mona', 'Gynaekologie', 'Frankfurter Allee 100', 10247, 'Berlin'),
(3017, 'Deuten', 'Ann', 'Kinderheilkunde', 'Stuewestr. 28', 12621, 'Berlin');

-- --------------------------------------------------------

--
-- Table structure for table `T_Aerzte_Impfungen`
--

DROP TABLE IF EXISTS `T_Aerzte_Impfungen`;
CREATE TABLE `T_Aerzte_Impfungen` (
  `aid` int(4) NOT NULL,
  `iid` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `T_Aerzte_Impfungen`
--

INSERT INTO `T_Aerzte_Impfungen` (`aid`, `iid`) VALUES
(3005, 4001),
(3009, 4001),
(3010, 4001),
(3002, 4002),
(3005, 4002),
(3010, 4002),
(3014, 4002),
(3005, 4003),
(3007, 4003),
(3010, 4003),
(3002, 4004),
(3004, 4004),
(3010, 4004),
(3017, 4004),
(3001, 4005),
(3002, 4005),
(3004, 4005),
(3005, 4005),
(3007, 4005),
(3009, 4005),
(3010, 4005),
(3014, 4005),
(3017, 4005),
(3004, 4006),
(3009, 4006),
(3001, 4007),
(3010, 4007),
(3001, 4008),
(3009, 4008),
(3010, 4008),
(3001, 4009),
(3010, 4009),
(3002, 4010),
(3003, 4010),
(3004, 4010),
(3010, 4010),
(3014, 4010),
(3016, 4010),
(3003, 4011),
(3009, 4011),
(3010, 4011),
(3002, 4012),
(3004, 4012),
(3010, 4012),
(3017, 4012);

-- --------------------------------------------------------

--
-- Table structure for table `T_Erreger`
--

DROP TABLE IF EXISTS `T_Erreger`;
CREATE TABLE `T_Erreger` (
  `eid` int(4) NOT NULL,
  `ename` varchar(30) NOT NULL,
  `ekrankheit` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `T_Erreger`
--

INSERT INTO `T_Erreger` (`eid`, `ename`, `ekrankheit`) VALUES
(1001, 'Poliovirus', 'Kinderlaehmung'),
(1003, 'Corynebakterium', 'Diphtherie'),
(1004, 'Paramyxovirus', 'Mumps'),
(1005, 'Clostridium tetani Bakterium', 'Wundstarrkrampf (Tetanus)'),
(1006, 'Bordetella pertussis Bakterium', 'Keuchhusten'),
(1007, 'Gelbfieber-Virus', 'Gelbfieber'),
(1008, 'Vibrio cholerae Bakterium', 'Cholera'),
(1009, 'Meningokokken Bakterium', 'Hirnhautentzuendung'),
(1010, 'FSME-Virus', 'Fruehsommer-Meningoenzephalitis'),
(1011, 'Paramyxovirus', 'Masern'),
(1012, 'Rubivirus', 'Roeteln'),
(1013, 'Japan.-Enzephalitis-Virus', 'Japanische Enzephalitis'),
(1014, 'Mycobacterium tuberculosis', 'Tuberkulose'),
(1015, 'H1N1-Virus', 'Schweine-Grippe'),
(1016, 'HIV', 'AIDS');

-- --------------------------------------------------------

--
-- Table structure for table `T_Impfstoffe`
--

DROP TABLE IF EXISTS `T_Impfstoffe`;
CREATE TABLE `T_Impfstoffe` (
  `isid` int(4) NOT NULL,
  `isbezeichnung` varchar(30) NOT NULL,
  `ishersteller` varchar(30) DEFAULT NULL,
  `iskombi` tinyint(1) DEFAULT NULL,
  `iid` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `T_Impfstoffe`
--

INSERT INTO `T_Impfstoffe` (`isid`, `isbezeichnung`, `ishersteller`, `iskombi`, `iid`) VALUES
(2001, 'Imovax Polio', 'Kohlpharma GmbH', 0, 4001),
(2002, 'Imovax Polio', 'EURIM-PHARM Arzneimittel', 0, 4001),
(2003, 'Boostrix Polio', 'EURIM-PHARM Arzneimittel', 1, 4002),
(2004, 'Diphterie Adsorbat', 'Novartis Vaccines', 0, 4003),
(2005, 'Covaxis', 'Sanofi Pasteur', 1, 4004),
(2006, 'Priorix', 'Veron Pharma', 1, 4005),
(2007, 'Priorix', 'Pharma Gerke GmbH', 1, 4005),
(2008, 'diTe Booster', 'Statens Serum Institut', 1, 4006),
(2009, 'Infanrix penta', 'Glaxo Smith Kline', 1, 4002),
(2010, 'Ixiaro', 'Intercell AG', 0, 4007),
(2011, 'Stamaril', 'Sanofi Pasteur', 0, 4008),
(2012, 'Dukoral', 'SBL Vaccin', 0, 4009),
(2013, 'Pandemrix', 'Glaxo Smith Kline', 0, 4010),
(2014, 'Celvapan', 'Baxter', 0, 4010),
(2015, 'Mencevax ACWY', 'Glaxo Smith Kline', 0, 4011),
(2016, 'Meningitec', 'Kohlpharma GmbH', 0, 4011),
(2017, 'Encepur', 'Novartis Vaccines', 0, 4012),
(2018, 'Encepur', 'EMRA-MED', 0, 4012);

-- --------------------------------------------------------

--
-- Table structure for table `T_Impfstoffe_Erreger`
--

DROP TABLE IF EXISTS `T_Impfstoffe_Erreger`;
CREATE TABLE `T_Impfstoffe_Erreger` (
  `isid` int(4) NOT NULL,
  `eid` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `T_Impfstoffe_Erreger`
--

INSERT INTO `T_Impfstoffe_Erreger` (`isid`, `eid`) VALUES
(2001, 1001),
(2002, 1001),
(2003, 1001),
(2003, 1003),
(2003, 1005),
(2003, 1006),
(2004, 1003),
(2005, 1003),
(2005, 1005),
(2005, 1006),
(2006, 1004),
(2006, 1011),
(2006, 1012),
(2007, 1004),
(2007, 1011),
(2007, 1012),
(2008, 1003),
(2008, 1005),
(2009, 1001),
(2009, 1003),
(2009, 1005),
(2009, 1006),
(2010, 1013),
(2011, 1007),
(2012, 1008),
(2013, 1015),
(2014, 1015),
(2015, 1009),
(2016, 1009),
(2017, 1010),
(2018, 1010);

-- --------------------------------------------------------

--
-- Table structure for table `T_Impfungen`
--

DROP TABLE IF EXISTS `T_Impfungen`;
CREATE TABLE `T_Impfungen` (
  `iid` int(4) NOT NULL,
  `name` varchar(30) NOT NULL,
  `gueltigjahre` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `T_Impfungen`
--

INSERT INTO `T_Impfungen` (`iid`, `name`, `gueltigjahre`) VALUES
(4001, 'IPV (Polio)', 10),
(4002, 'IPV-D-T-aP (PolDiphTetKeu)', 10),
(4003, 'D/d (Diph)', 10),
(4004, 'D-T-aP (DiphTetKeu)', 10),
(4005, 'MMR (MasMumRoe)', 10),
(4006, 'D-T (DiphTet)', 10),
(4007, 'JapE', 10),
(4008, 'Gelb', 10),
(4009, 'Cho', 10),
(4010, 'H1N1', 10),
(4011, 'Hirn', 10),
(4012, 'Fsme', 10);

-- --------------------------------------------------------

--
-- Table structure for table `T_Patienten`
--

DROP TABLE IF EXISTS `T_Patienten`;
CREATE TABLE `T_Patienten` (
  `pid` int(4) NOT NULL,
  `name` varchar(30) NOT NULL,
  `vorname` varchar(20) NOT NULL,
  `birthday` date NOT NULL,
  `street` varchar(30) DEFAULT NULL,
  `plz` int(5) DEFAULT NULL,
  `town` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `T_Patienten`
--

INSERT INTO `T_Patienten` (`pid`, `name`, `vorname`, `birthday`, `street`, `plz`, `town`) VALUES
(5001, 'Stjepanovic', 'Lara', '1932-05-23', 'Adolfsecker Weg 15', 15319, 'Berlin'),
(5002, 'Mbathie', 'Eileen', '1957-02-27', 'Dachdecker Weg 8', 16515, 'Oranienburg'),
(5003, 'Kraemer', 'Joschua', '1991-07-14', 'Am Kurpark 4', 16515, 'Oranienburg'),
(5004, 'Arndt', 'Philipp', '1993-06-15', 'Boederstrasse 11', 10627, 'Berlin'),
(5005, 'Blum', 'Nils', '1992-03-20', 'Badstrasse 13', 10433, 'Berlin'),
(5006, 'Frey', 'Jacqueline', '1992-05-28', 'Schulstrasse 23', 10422, 'Berlin'),
(5007, 'Hartmann', 'Nils', '1992-08-17', 'Poomernstrasse 9', 14401, 'Potsdam'),
(5008, 'Atzorn', 'Natalie', '1973-05-04', 'Hauptstrasse 9', 14432, 'Potsdam'),
(5009, 'Dietrich', 'Sebastian', '1982-06-10', 'Goethestrasse 20', 14423, 'Potsdam'),
(5010, 'Hackl', 'Julian', '1957-08-14', 'Siemensstrasse 9', 14482, 'Potsdam'),
(5011, 'Klugg', 'Frederik', '1945-10-15', 'Schollstrasse 12', 14481, 'Potsdam'),
(5012, 'Wagner', 'Dominic', '1965-05-05', 'Hollerbergstrasse 10', 14482, 'Potsdam'),
(5013, 'Zorn', 'Angelina', '1967-10-01', 'Hollerbergstrasse 22', 14482, 'Potsdam'),
(5014, 'Velte', 'Maik', '1972-09-18', 'In den Krautaeckern 26', 14442, 'Potsdam'),
(5015, 'Ruth', 'Sarah', '1975-11-23', 'In den Aeckern 1', 14406, 'Potsdam'),
(5016, 'Brandsched', 'Nadine', '1977-10-08', 'Hauptstrasse 78', 14406, 'Potsdam'),
(5017, 'Baron', 'Alexandra', '1980-06-15', 'Muehlweg 27', 12051, 'Berlin'),
(5018, 'Mohr', 'Laura', '1991-06-28', 'Moellerstrasse 34', 12043, 'Berlin'),
(5019, 'Scheppner', 'Fabiano', '1989-09-06', 'Britzer Damm 35', 12056, 'Berlin'),
(5020, 'Zahn', 'Alisa', '1989-08-11', 'Panoramastrasse 10', 12347, 'Berlin'),
(5021, 'Mayer', 'Yannick', '1993-04-21', 'Pestalozzistrasse 30', 12351, 'Berlin'),
(5022, 'Spiesbach', 'Johannes', '1992-12-28', 'Ritterstrasse 5', 16515, 'Oranienburg'),
(5023, 'Istanovic', 'Baris', '2001-06-24', 'Siedlungsstrasse 2', 16515, 'Oranienburg'),
(5024, 'Salta', 'Aylin', '2002-07-31', 'Siedlungsstrasse 4', 16515, 'Oranienburg'),
(5025, 'Krahl', 'Dennis', '2005-01-03', 'Ueber der Aar 15', 16515, 'Oranienburg'),
(5026, 'Becker', 'Sophia', '2008-05-18', 'Vor den Erlen 4', 12433, 'Berlin'),
(5027, 'Wolf', 'Saskia', '2009-07-12', 'Webergasse 1', 12342, 'Berlin'),
(5028, 'Radloff', 'Kati', '2000-01-02', '', 0, ''),
(5029, 'Becker', 'Carolina', '2000-01-03', '', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `T_Patienten_Impfungen`
--

DROP TABLE IF EXISTS `T_Patienten_Impfungen`;
CREATE TABLE `T_Patienten_Impfungen` (
  `pid` int(4) NOT NULL,
  `iid` int(4) NOT NULL,
  `aid` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `T_Patienten_Impfungen`
--

INSERT INTO `T_Patienten_Impfungen` (`pid`, `iid`, `aid`, `date`) VALUES
(5001, 4001, 3001, '2020-03-26'),
(5001, 4002, 3001, '2020-03-26'),
(5001, 4003, 3001, '2020-03-26'),
(5001, 4004, 3001, '2020-03-26'),
(5001, 4005, 3001, '2020-03-26'),
(5001, 4006, 3004, '2001-12-13'),
(5001, 4007, 3001, '2020-03-26'),
(5001, 4008, 3001, '2020-03-26'),
(5001, 4009, 3001, '2020-03-26'),
(5001, 4010, 3004, '2009-10-13'),
(5001, 4011, 3001, '2020-03-26'),
(5001, 4012, 3004, '2003-08-11'),
(5002, 4001, 3005, '1967-02-21'),
(5002, 4003, 3005, '1995-07-25'),
(5003, 4002, 3005, '1992-11-03'),
(5003, 4005, 3005, '1993-04-05'),
(5004, 4002, 3014, '1994-11-11'),
(5004, 4005, 3014, '1996-12-03'),
(5004, 4010, 3014, '2009-10-11'),
(5005, 4005, 3009, '1993-12-05'),
(5005, 4006, 3009, '1995-06-08'),
(5005, 4008, 3009, '2008-01-13'),
(5005, 4011, 3009, '2007-02-24'),
(5006, 4004, 3017, '1992-12-23'),
(5006, 4005, 3017, '1993-03-11'),
(5006, 4012, 3017, '2007-12-04'),
(5007, 4004, 3004, '1994-08-15'),
(5007, 4005, 3004, '1995-09-18'),
(5007, 4012, 3004, '2006-10-21'),
(5008, 4004, 3010, '2001-09-09'),
(5008, 4007, 3010, '2005-05-05'),
(5008, 4009, 3010, '2005-05-05'),
(5008, 4010, 3016, '2009-08-05'),
(5009, 4001, 3001, '2020-03-26'),
(5009, 4003, 3007, '1994-04-23'),
(5009, 4005, 3007, '1983-02-22'),
(5010, 4005, 3001, '1975-09-04'),
(5010, 4007, 3001, '2001-12-23'),
(5010, 4008, 3001, '2003-08-04'),
(5010, 4009, 3001, '2003-08-04'),
(5011, 4001, 3010, '1965-09-04'),
(5011, 4002, 3010, '1980-11-23'),
(5011, 4003, 3010, '1955-07-12'),
(5011, 4005, 3010, '1953-07-09'),
(5011, 4007, 3010, '2002-08-17'),
(5011, 4008, 3010, '2002-08-17'),
(5011, 4009, 3010, '2002-08-17'),
(5011, 4010, 3010, '2009-08-04'),
(5011, 4011, 3010, '1997-05-02'),
(5011, 4012, 3010, '1997-05-02'),
(5012, 4010, 3003, '2009-07-02'),
(5013, 4004, 3002, '1970-02-04'),
(5013, 4005, 3002, '1970-02-04'),
(5014, 4012, 3002, '1994-04-25'),
(5015, 4011, 3003, '1980-11-30'),
(5016, 4002, 3002, '1980-10-27'),
(5016, 4010, 3002, '2009-08-03'),
(5028, 4001, 3001, '2020-03-26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `T_Aerzte`
--
ALTER TABLE `T_Aerzte`
  ADD PRIMARY KEY (`aid`);

--
-- Indexes for table `T_Aerzte_Impfungen`
--
ALTER TABLE `T_Aerzte_Impfungen`
  ADD PRIMARY KEY (`aid`,`iid`),
  ADD KEY `iid` (`iid`);

--
-- Indexes for table `T_Erreger`
--
ALTER TABLE `T_Erreger`
  ADD PRIMARY KEY (`eid`);

--
-- Indexes for table `T_Impfstoffe`
--
ALTER TABLE `T_Impfstoffe`
  ADD PRIMARY KEY (`isid`),
  ADD KEY `iid` (`iid`);

--
-- Indexes for table `T_Impfstoffe_Erreger`
--
ALTER TABLE `T_Impfstoffe_Erreger`
  ADD KEY `isid` (`isid`),
  ADD KEY `eid` (`eid`);

--
-- Indexes for table `T_Impfungen`
--
ALTER TABLE `T_Impfungen`
  ADD PRIMARY KEY (`iid`);

--
-- Indexes for table `T_Patienten`
--
ALTER TABLE `T_Patienten`
  ADD PRIMARY KEY (`pid`),
  ADD UNIQUE KEY `pid` (`pid`);

--
-- Indexes for table `T_Patienten_Impfungen`
--
ALTER TABLE `T_Patienten_Impfungen`
  ADD PRIMARY KEY (`pid`,`iid`,`aid`),
  ADD KEY `iid` (`iid`),
  ADD KEY `aid` (`aid`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `T_Aerzte_Impfungen`
--
ALTER TABLE `T_Aerzte_Impfungen`
  ADD CONSTRAINT `aerzte_impfungen_ibfk_1` FOREIGN KEY (`aid`) REFERENCES `T_Aerzte` (`aid`),
  ADD CONSTRAINT `aerzte_impfungen_ibfk_2` FOREIGN KEY (`iid`) REFERENCES `T_Impfungen` (`iid`);

--
-- Constraints for table `T_Impfstoffe`
--
ALTER TABLE `T_Impfstoffe`
  ADD CONSTRAINT `impfstoffe_ibfk_1` FOREIGN KEY (`iid`) REFERENCES `T_Impfungen` (`iid`);

--
-- Constraints for table `T_Impfstoffe_Erreger`
--
ALTER TABLE `T_Impfstoffe_Erreger`
  ADD CONSTRAINT `impfstoffe_erreger_ibfk_1` FOREIGN KEY (`isid`) REFERENCES `T_Impfstoffe` (`isid`),
  ADD CONSTRAINT `impfstoffe_erreger_ibfk_2` FOREIGN KEY (`eid`) REFERENCES `T_Erreger` (`eid`);

--
-- Constraints for table `T_Patienten_Impfungen`
--
ALTER TABLE `T_Patienten_Impfungen`
  ADD CONSTRAINT `patienten_impfungen_ibfk_1` FOREIGN KEY (`pid`) REFERENCES `T_Patienten` (`pid`),
  ADD CONSTRAINT `patienten_impfungen_ibfk_2` FOREIGN KEY (`iid`) REFERENCES `T_Impfungen` (`iid`),
  ADD CONSTRAINT `patienten_impfungen_ibfk_3` FOREIGN KEY (`aid`) REFERENCES `T_Aerzte` (`aid`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
